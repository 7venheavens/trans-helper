import re
if __name__ == "__main__":
    import sys
    sys.path.append("..\\")
from classes.TItem import TItem



splitter = re.compile(r"""@([\w\.]*)(\s.*)""")
get_title = re.compile(r"""@(\S*)""")
data_re = re.compile(r"""(\S+)=((?:.(?!\s+(?:\S+)=))+.)""")

multi_fields = {"en", "jp", "fontsize", "font"}

# def process_entity_string(string):
#     print(string)
#     try:
#         entity, raw_data = splitter.findall(string)[0]
#     except:
#         entity, raw_data = get_title.findall(string)[0], ""
#     data = dict()
#     raw_data = data_re.findall(raw_data)
#     for key, raw_datum in raw_data:
#         if key in multi_fields:
#             data[key] = [i.strip() for i in raw_datum.split(",")]
#         else:
#             data[key] = raw_datum.strip()

#     return entity, data

whitespace = {" ", "\t", "\n"}
def process_entity_string_2(string):
    """
    processes an entity string of form r"@................."
    to obtain the entity and all tags
    """
    res = dict()
    entity = None
    cur = []
    index = 1
    string_length = len(string)
    while index < string_length:
        # find the first case of @entity, @ entity is not permitted
        if string[index] in whitespace and not entity:
            entity = "".join(cur).strip()
            if not entity:
                print("No entity found")
                raise
            cur = []
            entity_end = index
            continue
        cur.append(string[index])
        # if an assigner was found, find next assigner or end of text
        if string[index] == "=" and string[index-1] != "\\": #\\ handles escapes
            if string[index -1 ] in whitespace:
                raise
            #backtrace to find variable name
            var = None
            for i in range(index-1, entity_end -1, -1):
                if string[i] in whitespace:
                    start = i + 1
                    break
                # print(string[i: index])
            var = string[start:index]
            #forward search to find variable data
            end = None
            for i in range(index + 1, string_length):
                if i == string_length or end != None:
                    break
                # if an equals is found, backtrace and remove the previous word
                if string[i] == "=" and string[i-1] != "\\":
                    # same as earlier
                    if string[i -1] in whitespace:
                        raise
                    print("i-1, index+1: ", i-1, index+1)
                    for j in range(i-1, index + 1, -1):
                        # print("blah: ", string[index+1: j])
                        if string[j] in whitespace:
                            end = j
                            break
            # print(end)
            data = string[index + 1: end]
            # print(string[index+1:end])

            if (var and data):
                res[var] = data            
        index += 1
    if not entity:
        entity = "".join(cur)


    # print(res)
    return (entity, res)

def parse_translation(target):
    res = []
    cur_strs = []

    with open(target, encoding = "UTF-8") as f:
        for num, line in enumerate(f):
            try:
                if line.startswith("#"):
                    continue
                cur_str = line.split("#")[0].strip()
                if cur_str.startswith("@"): # Check if it's a new entity
                    if cur_strs:
                        res.append(process_entity_string_2(" ".join(cur_strs)))
                        # print(TItem(*res[-1]))
                    cur_strs = [cur_str]
                else:
                    if cur_str: #don't stick on if there isn't a base
                        cur_strs.append(cur_str)
            except:
                print("Failure at line {}".format(num))
                raise
        if cur_strs:
            # print("FINALE", cur_strs)
            res.append(process_entity_string_2(" ".join(cur_strs)))
    return [TItem(file, data_dict) for file, data_dict in res]


if __name__ == "__main__":
    test_data = "D:/Projects/Partials/VB_hypno/translations\\interface.xp3~\\sys_ui.txt"
    # for i in parse_translation(test_data):
    #     pass
    #     # print(i)

    test1 = TItem(*process_entity_string_2("@ui_dlog_bt_yes.png  jp=はい   en=Yes,no, maybe, nope  template=ui_dlog_bt_yes.png fontsize=40"))
    assert(test1.jp == ["はい"])
    assert(test1.en == ["Yes", "no", "maybe", "nope"])
    assert(test1.template == "ui_dlog_bt_yes.png")
    assert(test1.fontsize == [40])