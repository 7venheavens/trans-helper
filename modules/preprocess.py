# class TItem:
# 	def __init__(self, tup):
# 		tup_len = len(tup)
# 		if tup_len < 7:
# 			raise
# 		self.file, self.jp, self.en, self.template, self.template_type, self.fontsize, self.notes = tup
# 		# 
# 		self.jp = self.jp.split(",")
# 		self.en = self.en.split(",")
# 		self.fontsize = [int(i) for i in self.fontsize.split(",")]

# 	def __repr__(self):
# 		return "<TItem: file: '{}', jp: '{}', en: '{}', template: '{}', template_type: '{}', fontsize: '{}'' notes: '{}'>".format(self.file, self.jp, self.en, self.template, self.template_type, self.fontsize, self.notes)


# def read_translation(translation_tsv):
# 	res = []
# 	with open(translation_tsv, encoding = "UTF-8") as f:
# 		for line in f:
# 			if line[0] == "#" or line[0] == "\n":
# 				continue
# 			line = line.split("#")[0]
# 			tup = line.strip().split("\t")
# 			if len(tup) != 7:
# 				# print("SKIPPING ", tup)
# 				continue
# 			res.append(TItem(tup))
# 	return res

def read_template_config(template_cfg):
	def handle_coords(datum):
		print(datum)
		datum = datum.split(",")
		if len(datum) == 1:
			return (int(datum[0]), -1)
		else:
			return (int(datum[0]), int(datum[1]))
	res = dict()
	with open(template_cfg) as f:
		for line in f:
			line = line.split("#")[0]
			if line == "\n" or not line:
				continue
			template_name, *insert_points = line.strip().split("\t")
			insert_points = [handle_coords(i) for i in insert_points]
			res[template_name] = insert_points
	return res


if __name__ == "__main__":
	tsv = "D:/Projects/Partials/VB_hypno/translations/interface.xp3~/slg_ui.txt"
	for item in read_translation(tsv):
		print(item)
	template_cfg_tsv = "D:/Projects/Partials/VB_hypno/templates/interface.xp3~/slg_ui/config.tsv"
	for key, item in read_template_config(template_cfg_tsv).items():
		print(key, item)

