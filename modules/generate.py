from PIL import ImageFont, Image, ImageDraw
from collections import defaultdict
import copy

class FontHolder:
	def __init__(self):
		self.default_font = "data/fonts/Calibri.ttf"
		self.fonts = defaultdict(dict)
	def get_font(self, size, font = None):
		if not font:
			font = self.default_font
		font_dict = self.fonts[font]
		if size not in font_dict:
			font_dict[size] = ImageFont.truetype(font, size)
		return font_dict[size]
		
font_holder = FontHolder()
global_fontsize = 15

def create_from_template(t_item, template_path, outpath, insert_points, gamefile_path = None, template_type = "base",  fontsizes = [-1], ):
	# print(t_item.en)

	fontsizes = copy.copy(fontsizes)
	# Handle autoexpansion of single argument multiple slot cases
	if len(fontsizes) == 1 and len(insert_points) != 1:
		fontsizes = [fontsizes[0] for i in insert_points]

	for index, entry in enumerate(fontsizes):
		if entry == -1:
			fontsizes[index] = global_fontsize

	template = Image.open(template_path)

	if template_type == "overlay":
		assert(gamefile_path)
		original = Image.open(gamefile_path)
		
		try:
			template = Image.alpha_composite(original.convert("RGBA"), template.convert("RGBA"))
		except ValueError:
			print("Image mismatch.")
			print("original = {}, {}; template = {}, {}".format(*original.size, *template.size))
			print(original.mode, template.mode)
			raise
	d = ImageDraw.Draw(template)
	if not t_item.en:
		return
	# handle single insert multiple points
	if len(t_item.en) != len(insert_points):
		inserts = [t_item.en[0] for i in insert_points]
	else:
		inserts = t_item.en
	# print("INSERTS: ", inserts)
	# print("IPS: ", insert_points)
	# print("FS: ", fontsizes)
	for insert, (x, y), fontsize in zip(inserts, insert_points, fontsizes):
		# print(t_item.file, "originalxy:", x,y)
		# Handle insert centering
		w, h = d.textsize(insert, font = font_holder.get_font(fontsize))
		# Handle an issue with different heights caused by g et al
		_, h = d.textsize("a", font = font_holder.get_font(fontsize))
		# print("w, h: ", w, h)
		x = x - w//2
		# Handle the case of default height
		if y < 0:
			y = (template.height - h)//2
		else:
			y = y - h//2
		# print("DRAWING: ", t_item.file, insert, x, y)
		# outline, then draw the text
		d.text((x-1,y), insert, (0, 0, 0), font = font_holder.get_font(fontsize))	
		d.text((x+1,y), insert, (0, 0, 0), font = font_holder.get_font(fontsize))	
		d.text((x,y-1), insert, (0, 0, 0), font = font_holder.get_font(fontsize))	
		d.text((x,y+1), insert, (0, 0, 0), font = font_holder.get_font(fontsize))	

		d.text((x,y), insert, (221, 209, 194), font = font_holder.get_font(fontsize))	

	template.save(outpath)

