from PIL import Image, ImageFont, ImageDraw

image = Image.new("RGB", (200, 50), (255, 255, 255))

font = ImageFont.truetype("calibri.ttf", 20)

i1 = image.copy()
i2 = image.copy()

d1 =ImageDraw.Draw(i1)
d2 =ImageDraw.Draw(i2)

text = "testingtesttingtesting"
print(d1.textsize(text))

i1.save("output/d1.png")

cx, cy = i1.width//2, i1.height//2
w, h = d1.textsize(text)
d2.text((cx - w//2, cy-h//2), text, (0, 0, 0))
i2.save("output/d2.png")