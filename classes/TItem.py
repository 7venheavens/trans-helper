class TItem:
	def __init__(self, file, data_dict):
		self.file = file
		keys = [("jp", "multi"), ("en", "multi"), ("template", "single"), ("template_type", "single"), ("fontsize", "multi"), ("notes", "single")]
		for key, field_tag in keys:
			if key in data_dict:
				if field_tag == "multi":
					setattr(self, key, [i.strip() for i in data_dict[key].split(",")])
				else:
					setattr(self, key, data_dict[key].strip())
			else:
				setattr(self, key, None)
		try:
			self.fontsize = [int(i) for i in self.fontsize]
		except:
			self.fontsize = [-1]

	def __repr__(self):
		return "<TItem: file: '{}', jp: '{}', en: '{}', template: '{}', template_type: '{}', fontsize: '{}'' notes: '{}'>".format(self.file, self.jp, self.en, self.template, self.template_type, self.fontsize, self.notes)