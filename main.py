import os
import shutil
from PIL import Image, ImageDraw, ImageFont
from filecmp import cmp

from modules import preprocess, generate, parse_translation

# Constants
BASE_DIR        = "D:/Projects/Partials/VB_hypno/"
GAMEFILE_DIR    = os.path.join(BASE_DIR, "vb_files")
TEMPLATE_DIR    = os.path.join(BASE_DIR, "templates")
TRANSLATION_DIR = os.path.join(BASE_DIR, "translations")
OUTPUT_DIR      = os.path.join(BASE_DIR, "output")
PATCH_DIR    = os.path.join(BASE_DIR, "patch")

def create_mappings(base_dir, gamefile_dir, template_dir, translation_dir, output_dir = OUTPUT_DIR):
    """
    Args
        base_dir
        gamefile_dir
        template_dir
        translation_dir
        output_dir
    """

    # Find which entries have translations and templates
    # Translations -> items with translations, templates -> full path to available templates, gamefiles -> full path to gamefiles
    # Collect all translated items
    translations = dict()
    for xp3_dir in os.listdir(translation_dir):
        translations[xp3_dir] = dict()
        for translation_file in os.listdir(os.path.join(translation_dir, xp3_dir)):
            print("PARSING: ", os.path.join(translation_dir, xp3_dir, translation_file))
            try:
                translations[xp3_dir][os.path.splitext(translation_file)[0]] = parse_translation.parse_translation(os.path.join(translation_dir, xp3_dir, translation_file))
            except:
                print("FAILED TO PARSE: ", os.path.join(translation_dir, xp3_dir, translation_file))
                print(parse_translation.parse_translation(os.path.join(translation_dir, xp3_dir, translation_file)))
                raise

    gamefiles = dict()
    for xp3_dir in os.listdir(gamefile_dir):
        gamefiles[xp3_dir] = dict()
        for category in os.listdir(os.path.join(gamefile_dir, xp3_dir)):
            gamefiles[xp3_dir][category] = dict()
            if not os.path.isdir(os.path.join(gamefile_dir, xp3_dir, category)):
                continue
            for gamefile_file in os.listdir(os.path.join(gamefile_dir, xp3_dir, category)):
                if not gamefile_file.endswith("png"):
                    continue
                gamefiles[xp3_dir][category][gamefile_file] = os.path.join(gamefile_dir, xp3_dir, category, gamefile_file)

    templates = dict()
    for template_file in os.listdir(template_dir):
        if not template_file.endswith("png"):
            if template_file == "config.tsv":
                templates["config"] = preprocess.read_template_config(os.path.join(template_dir, template_file))
            continue

        templates[template_file] = os.path.join(template_dir, template_file)

    return translations, gamefiles, templates

def compile_images(translations, gamefiles, templates, outdir = OUTPUT_DIR):
    skipped = []
    for xp3_dir in translations:
        for category in translations[xp3_dir]:
            if not os.path.exists(os.path.join(outdir, xp3_dir, category)):
                os.makedirs(os.path.join(outdir, xp3_dir, category))
            for t_item in translations[xp3_dir][category]:
                # print(t_item)
                # print("ITEM: ", t_item.file)         
                cur_file = t_item.file
                print("DATUM: xp3: {}, category: {}, cur_file: {}, template: {}".format(xp3_dir, category, cur_file, t_item.template))
                cur_gamefiles = gamefiles[xp3_dir][category]


                if t_item.template not in templates or cur_file not in cur_gamefiles:
                    print("Skipping: {}, template found: {}, gamefile found: {}".format(os.path.join(xp3_dir, category, cur_file), 
                                                                                        t_item.template in templates, 
                                                                                        cur_file in cur_gamefiles ))
                    skipped.append("\t".join(str(i) for i in [os.path.join(xp3_dir, category, cur_file), 
                                              t_item.template in templates, 
                                              cur_file in cur_gamefiles ]))
                    continue
                template_path = templates[t_item.template]
                gamefile_path = cur_gamefiles[cur_file]
                print(template_path, gamefile_path)
                print(t_item)
                generate.create_from_template(t_item, template_path, 
                                              outpath = os.path.join(outdir, xp3_dir, category, cur_file),
                                              insert_points = templates["config"][t_item.template],
                                              gamefile_path = gamefile_path, template_type = t_item.template_type, 
                                              fontsizes = t_item.fontsize)
    with open("skipped.txt", "w", encoding = "UTF-8") as f:
        f.write("# path\thas_template\thas_gamefile")
        for i in skipped:
            f.write(i + "\n")
    return 0

def compile_targets():
    pass

def cleanup(outdir= OUTPUT_DIR, patch_dir = PATCH_DIR):
    shutil.rmtree(os.path.join(outdir, ""), ignore_errors = True)
    shutil.rmtree(os.path.join(patch_dir, ""), ignore_errors = True)



def compile_patch(gamefile_dir, outdir, patch_dir, edit_dir = None):
    """
    Compiles output images and gamefiles into a prepatch state
    Arguments
        gamefile_dir
        outdir
        patch_dir

    """
    for xp3_dir in os.listdir(gamefile_dir):
        for category in os.listdir(os.path.join(gamefile_dir, xp3_dir)):
            if not os.path.exists(os.path.join(patch_dir, xp3_dir, category)):
                os.makedirs(os.path.join(patch_dir, xp3_dir, category))
            #current gamefile and outfile and patch directory paths
            cg_dir = os.path.join(gamefile_dir, xp3_dir, category)
            co_dir = os.path.join(outdir, xp3_dir, category)
            cp_dir = os.path.join(patch_dir, xp3_dir, category)
            print("Processing: ", cg_dir)
            if not os.path.isdir(cg_dir):
                continue
            for file in os.listdir(cg_dir):
                cg_file = os.path.join(cg_dir, file)
                co_file = os.path.join(co_dir, file)
                cp_file = os.path.join(cp_dir, file)
                if os.path.exists(co_file):
                    if os.path.exists(cp_file) and cmp(cp_file, co_file):
                        continue
                    shutil.copy(co_file, cp_file)
                else:
                    if os.path.exists(cp_file) and cmp(cp_file, cg_file):
                        continue
                    shutil.copy(cg_file, cp_file)







if __name__ == "__main__":
    ts, gf, te = create_mappings(BASE_DIR, GAMEFILE_DIR, TEMPLATE_DIR, TRANSLATION_DIR)
    # print(gf["interface.xp3~"]["slg_ui"]["rDraft_bt33.png"])
    compile_images(ts, gf, te)
    # compile_patch(GAMEFILE_DIR, OUTPUT_DIR, PATCH_DIR)
    # cleanup()



